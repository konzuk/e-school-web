

/*
 * Inspinia js helpers:
 *
 * correctHeight() - fix the height of main wrapper
 * detectBody() - detect windows size
 * smoothlyMenu() - add smooth fade in/out on navigation show/ide
 *
 */

declare var jQuery: any;

export function correctHeight() {

  const pageWrapper = jQuery('#page-wrapper');
  const navbarHeight = jQuery('nav.navbar-default').height();
  const wrapperHeight = pageWrapper.height();
  const windowHeight = jQuery(window).height();

  const videoWrapper = jQuery('#video-wrapper');
  const videoPlayer = jQuery('#video-player');

  if (videoWrapper && videoPlayer) {
    const hei = (videoWrapper.width() * 9 / 16);
    const wid = videoWrapper.width();
    videoPlayer.css('height', hei + 'px');
    videoPlayer.css('width', wid + 'px');
  }

  if (navbarHeight > wrapperHeight) {
    pageWrapper.css('min-height', navbarHeight + 'px');
  }

  if (navbarHeight <= wrapperHeight) {
    if (navbarHeight < jQuery(window).height()) {
      pageWrapper.css('min-height', jQuery(window).height() + 'px');
    } else {
      pageWrapper.css('min-height', navbarHeight + 'px');
    }
  }

  if (jQuery('body').hasClass('fixed-nav')) {
    if (navbarHeight > wrapperHeight) {
      pageWrapper.css('min-height', navbarHeight - 60 + 'px');

      // if (jQuery('body').hasClass('fixed-sidebar')) {
      //   jQuery('.wrapper-content').slimscroll({
      //     height: navbarHeight - 20 + 'px'
      //   });
      // }
    } else {
      pageWrapper.css('min-height', jQuery(window).height() - 60 + 'px');

      // if (jQuery('body').hasClass('fixed-sidebar')) {
      //   jQuery('.wrapper-content').slimscroll({
      //     height: jQuery(window).height() - 60 - 20 + 'px'
      //   });
      // }
    }
  }

}

export function detectBody(globalVar) {
  if (jQuery(document).width() < 769) {
    jQuery('body').addClass('body-small');
    globalVar.isSmallBody = true;
  } else {
    jQuery('body').removeClass('body-small');
    globalVar.isSmallBody = false;
  }
}

export function smoothlyMenu() {
  if (!jQuery('body').hasClass('mini-navbar') || jQuery('body').hasClass('body-small')) {
    // Hide menu in order to smoothly turn on when maximize menu
    jQuery('#side-menu').hide();
    // For smoothly turn on menu
    setTimeout(
      function () {
        jQuery('#side-menu').fadeIn(400);
      }, 200);
  } else if (jQuery('body').hasClass('fixed-sidebar')) {
    jQuery('#side-menu').hide();
    setTimeout(
      function () {
        jQuery('#side-menu').fadeIn(400);
      }, 100);
  } else {
    // Remove all inline style from jquery fadeIn function to reset menu state
    jQuery('#side-menu').removeAttr('style');
  }
}
