import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, RouteReuseStrategy } from '@angular/router';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';


import { RxCacheService } from 'ngx-rxcache';


import { SocialLoginModule, AuthServiceConfig, LoginOpt } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AppComponent } from 'src/app/app.component';
import { LayoutsModule } from 'src/app/components/common/layouts/layouts.module';
import { LoginModule } from 'src/app/components/login/login.module';
import { TeacherModule } from 'src/app/components/teacher/teacher.module';
import { VideoModule } from 'src/app/components/video/video.module';
import { ROUTES } from 'src/app/app.routes';
import { CustomReuseStrategy } from 'src/app/services/custom-reuse-strategy';
import { DashboardModule } from 'src/app/components/dashboard/dashboard.module';


// App views

// App modules/components


const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('686632653491-8bep9kigmut0ojg8dksc3d2qsmsp9v71.apps.googleusercontent.com')
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    SocialLoginModule,
    RouterModule.forRoot(ROUTES),

    LayoutsModule,

    LoginModule,
    DashboardModule,
    TeacherModule,
    VideoModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: AuthServiceConfig, useFactory: provideConfig },
    { provide: RouteReuseStrategy, useClass: CustomReuseStrategy },
    RxCacheService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() { }
}

