import { Routes } from '@angular/router';
import { BasicLayoutComponent } from 'src/app/components/common/layouts/basicLayout.component';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { BlankLayoutComponent } from 'src/app/components/common/layouts/blankLayout.component';
import { LoginComponent } from 'src/app/components/login/login.component';




export const ROUTES: Routes = [
  // Main redirect
  { path: '', redirectTo: 'app/dashboard', pathMatch: 'full' },
  // App views
  {
    path: 'app', component: BasicLayoutComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent }
    ],
    data: { shouldDetach: true, parentKey: 'app' }
  },
  {
    path: '', component: BlankLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
    ]
  },
  { path: '**', redirectTo: 'app/dashboard' },
];




  // {
  //   path: 'dashboards', component: BasicLayoutComponent,
  //   children: [
  //     {path: 'dashboard1', component: Dashboard1Component},
  //     {path: 'dashboard2', component: Dashboard2Component},
  //     {path: 'dashboard3', component: Dashboard3Component},
  //     {path: 'dashboard4', component: Dashboard4Component},
  //     {path: 'dashboard5', component: Dashboard5Component}
  //   ]
  // },
  // {
  //   path: 'dashboards', component: TopNavigationLayoutComponent,
  //   children: [
  //     {path: 'dashboard41', component: Dashboard41Component}
  //   ]
  // },
