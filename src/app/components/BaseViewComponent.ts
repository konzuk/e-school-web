import { MyEventService } from 'src/app/services/my-event.service';

export abstract class BaseViewComponent {
    abstract handleData(arg0: any): any;
    abstract ngOnReInit();
    abstract key(): String;
    constructor(private myEvent: MyEventService) {
        myEvent.ResolveData.subscribe(myData => {
            const mKey = this.key();
            if (myData.key && myData.key === mKey) {
                this.handleData(myData.data);
            }
        });
        myEvent.AfterRefreshTokenEvent.subscribe(key => {
            const mKey = this.key();
            if (key && key === mKey) {
                this.ngOnReInit();
            }
        });
    }

}
