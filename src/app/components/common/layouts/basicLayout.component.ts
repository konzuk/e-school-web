import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
import { Globals } from 'src/app/app.globals';
import { detectBody, correctHeight } from 'src/app/app.helpers';

declare var jQuery: any;

@Component({
  selector: 'app-basic',
  templateUrl: 'basicLayout.template.html'
})
export class BasicLayoutComponent implements OnInit {

  constructor(private globalVar: Globals) { }

  public ngOnInit(): any {

    jQuery('body').addClass('fixed-sidebar');
    jQuery('body').addClass('fixed-nav');
    jQuery('body').addClass('fixed-nav-basic');

    detectBody(this.globalVar);
    correctHeight();
  }



  @HostListener('window:resize', ['$event'])
  public onResize() {
    detectBody(this.globalVar);
    correctHeight();
  }

}
