import { Injectable } from '@angular/core';
import { RxCacheService } from 'ngx-rxcache';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/services/api.service';
import { MyEventService } from 'src/app/services/my-event.service';
import { Globals } from 'src/app/app.globals';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends ApiService {


  serviceKey(): String { return 'dashboard'; }

  constructor(http: HttpClient, cache: RxCacheService, myEvent: MyEventService, globalVar: Globals) {
    super(http, cache, myEvent, globalVar);
  }

  public getData(): String {
    const headers = this.secureHeader();
    const res = this.getService('dashboard', headers);
    res.subscribe();
    return this.serviceKey();
  }
}
