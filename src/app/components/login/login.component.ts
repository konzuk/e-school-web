import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService, SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { LoginService } from 'src/app/components/login/login.service';
import { Globals } from 'src/app/app.globals';

@Component({
  selector: 'app-login',
  templateUrl: 'login.template.html'
})
export class LoginComponent implements OnInit, AfterViewInit {

  ngAfterViewInit(): void {
    this.login.logOutOauth();
  }

  ngOnInit(): void {

    this.authService.authState.subscribe((user) => {
      if (user && user instanceof SocialUser && user.id && this.globalVar.requestGoogleLogin) {
        this.signInGoogle(user);
        this.authService.signOut();
        this.globalVar.requestGoogleLogin = false;
      }
    });
  }

  constructor(private login: LoginService, private authService: AuthService, private globalVar: Globals) { }

  signIn(username, password, event): void {
    event.preventDefault();
    this.login.passwordOauth(username, password);
  }

  signInGoogle(user: SocialUser): void {
    event.preventDefault();
    this.login.thirdPartyOauth(user);
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.globalVar.requestGoogleLogin = true;
  }

}
