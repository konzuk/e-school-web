import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RxCacheService, RxCacheItem } from 'ngx-rxcache';
import { Globals } from 'src/app/app.globals';
import { Router } from '@angular/router';
import { SocialUser } from 'angularx-social-login';
import { ApiService } from 'src/app/services/api.service';
import { AccessTokenKey, RefreshTokenKey, clientId, CleintSecret } from 'src/environments/environment';
import { MyEventService } from 'src/app/services/my-event.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends ApiService {

  tokenAccessItem: RxCacheItem<string> = this.cache.get({ id: AccessTokenKey, sessionStorage: true });
  tokenRefreshItem: RxCacheItem<string> = this.cache.get({ id: RefreshTokenKey, localStorage: true });

  constructor(http: HttpClient, cache: RxCacheService, myEvent: MyEventService, globalVar: Globals, private router: Router) {
    super(http, cache, myEvent, globalVar);

  }

  serviceKey(): String { return 'oauth-api'; }

  public passwordOauth(username, password) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    const body = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('grant_type', 'password')
      .set('client_id', clientId)
      .set('client_secret', CleintSecret);
    const res = this.postService('oauth/token', body, headers);
    res.subscribe(this.handleOauthSuccess.bind(this), this.handleOauthFail.bind(this));
  }

  public thirdPartyOauth(user: SocialUser) {
    if (user) {
      const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
      const body = new HttpParams()
        .set('userId', user.id)
        .set('firstname', user.firstName)
        .set('lastname', user.lastName)
        .set('username', user.name)
        .set('email', user.email)
        .set('photoUrl', user.photoUrl)
        .set('type', user.provider)
        .set('client_id', clientId)
        .set('client_secret', CleintSecret);
      const res = this.postService('oauth/third-party', body, headers);
      res.subscribe(this.handleOauthSuccess.bind(this), this.handleOauthFail.bind(this));
    }
  }
  public logOutOauth() {

    const tokenRefresh = this.getCache(RefreshTokenKey);
    if (tokenRefresh) {
      const headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      });
      const body = new HttpParams()
        .set('token', tokenRefresh);
      const res = this.postService('oauth/logout', body, headers);
      res.subscribe(data => { this.resetToken(); });

    }
  }

  public refreshTokenOauth() {

    const tokenRefresh = this.getCache(RefreshTokenKey);
    if (tokenRefresh) {
      const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
      const body = new HttpParams()
        .set('refresh_token', tokenRefresh)
        .set('grant_type', 'refresh_token')
        .set('client_id', clientId)
        .set('client_secret', CleintSecret);
      const res = this.postService('oauth/token', body, headers);
      res.subscribe(this.handleOauthSuccess.bind(this), this.handleOauthFail.bind(this));
    } else {
      this.handleOauthFail(null);
    }
  }

  private saveToken(AccessToken, RefreshToken) {
    this.tokenAccessItem.update(AccessToken);
    this.tokenRefreshItem.update(RefreshToken);

  }

  private resetToken() {
    this.tokenAccessItem.reset();
    this.tokenRefreshItem.reset();
  }

  private handleOauthSuccess(res) {
    this.saveToken(res.access_token, res.refresh_token);

    if (this.globalVar.previousUrl) {
      this.router.navigate([this.globalVar.previousUrl]);
    } else {
      this.router.navigate(['']);
    }

    if (this.globalVar.isRefreshToken) {
      this.myEvent.AfterRefreshTokenEvent.next(this.globalVar.currentServiceKey);
      this.globalVar.isRefreshToken = false;
    }
  }
  private handleOauthRefresh(res) {
  }
  private handleOauthFail(res) {
    this.router.navigate([this.globalVar.loginUrl]);
  }
}
