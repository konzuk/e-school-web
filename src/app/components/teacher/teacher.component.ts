import { Component, OnInit } from '@angular/core';
import { MyEventService } from 'src/app/services/my-event.service';
import { trigger, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  constructor(private myEvent: MyEventService) { }

  IsPlay = false;
  VideoPlayClass = 'col-12';
  VideoListClass = 'col-12';

  ngOnInit() {
    this.myEvent.playVideo.subscribe((videoId) => {
      this.IsPlay = true;
      this.VideoPlayClass = 'col-xl-9 col-12 col-sm-12';
      this.VideoListClass = 'col-xl-3 col-12 col-sm-12';
    });
  }

}
