import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherComponent } from 'src/app/components/teacher/teacher.component';
import { TeacherListComponent } from 'src/app/components/teacher/teacher-list/teacher-list.component';
import { PageHeadingComponent } from 'src/app/components/common/page-heading/page-heading.component';
import { VideoPlayComponent } from 'src/app/components/teacher/video-play/video-play.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  imports: [
    CommonModule, BrowserAnimationsModule
  ],
  declarations: [
    TeacherComponent,
    TeacherListComponent,
    VideoPlayComponent,
    PageHeadingComponent
  ],
  exports: [
    TeacherComponent,
    TeacherListComponent,
    VideoPlayComponent,
    PageHeadingComponent
  ],
  entryComponents: [TeacherComponent]


})
export class TeacherModule {

  constructor() {

  }
}
