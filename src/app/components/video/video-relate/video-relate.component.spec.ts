import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoRelateComponent } from './video-relate.component';

describe('VideoRelateComponent', () => {
  let component: VideoRelateComponent;
  let fixture: ComponentFixture<VideoRelateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoRelateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoRelateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
