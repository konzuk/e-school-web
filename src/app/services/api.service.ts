

import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { RxCacheService, RxCacheItem } from 'ngx-rxcache';
import { MyEventService } from 'src/app/services/my-event.service';
import { Globals } from 'src/app/app.globals';
import { AccessTokenKey, apiUrl } from 'src/environments/environment';

​export abstract class ApiService {
  constructor(
    protected http: HttpClient,
    protected cache: RxCacheService,
    protected myEvent: MyEventService,
    protected globalVar: Globals) {
  }


  abstract serviceKey(): String;


  private handleRefreshToken() {
    this.myEvent.CallRefreshTokenEvent.next();
    this.globalVar.isRefreshToken = true;
    this.globalVar.refreshTokenOnError = false;
    this.globalVar.currentServiceKey = this.serviceKey();
  }

  handleError(res: HttpErrorResponse | any) {
    this.myEvent.ToogleSpinnerEvent.next(false);
    if (res.status === 401 && ((res.error && res.error.error === 'invalid_token') || this.globalVar.refreshTokenOnError))  {
      this.handleRefreshToken();
    } else {
      console.error(res);
    }
    return observableThrowError(res);
  }
  handleSuccess(data) {
    this.myEvent.ToogleSpinnerEvent.next(false);
    const key = this.serviceKey();
    const myData = { key: key, data: data.data };

    this.myEvent.ResolveData.next(myData);

    return data;
  }

  protected getCache(key: string) {
    const exists = this.cache.exists(key);
    if (exists) {
      const item: RxCacheItem<string> = this.cache.get(key);
      return item.value$.getValue();
    }
    return null;
  }

  protected secureHeader(): HttpHeaders {
    const tokenAccess = this.getCache(AccessTokenKey);
    if (tokenAccess) {

      return new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + tokenAccess
      });

    } else {
      this.globalVar.refreshTokenOnError = true;
      return null;
    }
  }

  protected postService(route, body: any | null = null, headers: any | null = null) {
    this.myEvent.ToogleSpinnerEvent.next(true);

    let _headers = {};
    if (headers) {
      _headers = { headers };
    }
    return this.http.post(apiUrl + route, body, _headers).pipe(
      map(this.handleSuccess.bind(this)),
      catchError(this.handleError.bind(this)));
  }

  protected getService(route, headers: any | null = null) {

    this.myEvent.ToogleSpinnerEvent.next(true);
    let _headers = {};
    if (headers) {
      _headers = { headers };
    }
    return this.http.get(apiUrl + route, _headers).pipe(
      map(this.handleSuccess.bind(this)),
      catchError(this.handleError.bind(this)));
  }
}

