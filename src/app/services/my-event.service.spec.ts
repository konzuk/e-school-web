import { TestBed, inject } from '@angular/core/testing';

import { MyEventService } from './my-event.service';

describe('MyEventService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyEventService]
    });
  });

  it('should be created', inject([MyEventService], (service: MyEventService) => {
    expect(service).toBeTruthy();
  }));
});
