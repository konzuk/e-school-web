import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})


export class MyEventService {
  constructor() { }
  public ToogleSpinnerEvent = new Subject<boolean>();
  public RouterNavigateEndEvent = new Subject<String>();
  public CallRefreshTokenEvent = new Subject();
  public ResolveData = new Subject<any>();
  public AfterRefreshTokenEvent = new Subject<String>();
  public playVideo = new Subject<String>();
}

