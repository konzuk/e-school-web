import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Globals } from 'src/app/app.globals';
import { smoothlyMenu } from 'src/app/app.helpers';
import { MyEventService } from 'src/app/services/my-event.service';
declare var jQuery: any;
@Injectable({
  providedIn: 'root'
})
export class MyRouterService {
  constructor(
    private router: Router,
    private myEvent: MyEventService,
    private globalVar: Globals
  ) {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.myEvent.RouterNavigateEndEvent.next(event.url);
          if (this.globalVar.isSmallBody) {
            jQuery('body').toggleClass('mini-navbar');
            smoothlyMenu();
          }

        }
      });
  }
}
